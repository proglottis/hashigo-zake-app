Privacy Policy
==============

This is the privacy policy for the Hashigo Zake Beer List, because Google
requires it.

The app collects no personally identifiable information from you. It does send
an anonymous token that is used to send notification to you, but that's it.

The server records your IP address as part of standard logging whenever you
download an updated menu. Hashigo Zake's webserver will see your IP address
in the same manner when you view anything other than the tap menu.

If you read this, let me know at robin@kallisti.net.nz. I reckon no one does.
