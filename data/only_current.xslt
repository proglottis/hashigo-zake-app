<?xml version="1.0" encoding="UTF-8"?>
<!-- only_current.xslt:
This takes the Hashigo XML feed that contains all historical entries, and only
outputs those that are marked as not not on tap (i.e. currently pouring, up
next.)
 -->
 
 <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 
 	<xsl:output method="xml" />
 	<xsl:strip-space elements="*" />
 	
 	<xsl:template match="/">
 		<Products>
 			<xsl:apply-templates select="Products" />
 		</Products>
 	</xsl:template>
 	
 	<xsl:template match="Products">
 		<xsl:apply-templates select="@Timestamp" />
 		<LastChange>
 			<xsl:apply-templates select="LastChange" />
 		</LastChange>
 		<Beers>
 			<xsl:apply-templates select="Beers/Product" />
 		</Beers>
 	</xsl:template>
 
 	<xsl:template match="@Timestamp">
 		<xsl:attribute name="Timestamp">
 			<xsl:value-of select="." />
 		</xsl:attribute>
 	</xsl:template>
 	
 	<xsl:template match="Product">
 		<xsl:if test="Pouring/text() != 'No'">
 			<Product>
 				<xsl:copy-of select="*" />
 			</Product>
 		</xsl:if>
 	</xsl:template>
 	
 	<xsl:template match="LastChange">
 		<xsl:copy-of select="*" />
 	</xsl:template>
 
 </xsl:stylesheet>