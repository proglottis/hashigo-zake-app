package nz.net.kallisti.android.hashigozake.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;

/**
 * This loads text from a URL, and when it's all done, it passes it on to a
 * Processor.
 * 
 * <b>Note:</b> based heavily of the android tutorial sample code
 * 
 * @author robin
 */
public class TextHTTPLoader extends AsyncTask<String, Integer, String> {

	protected Processor<String> callback;

	public void setCallback(Processor<String> processor) {
		callback = processor;
	}

	@Override
	public String doInBackground(String... urls) {
		if (callback == null) {
			throw new IllegalStateException(
					"Calling this when there is no callback set is silly");
		}
		try {
			return downloadUrl(urls[0]);
		} catch (IOException e) {
			callback.error(Processor.ERR_IO);
		}
		return null;
	}

	@Override
	protected void onPostExecute(String text) {
		if (text != null)
			callback.process(text);
	}

	private String downloadUrl(String inUrl) throws IOException {
		InputStream is = null;
		int maxLen = 1048 * 1024; // 1MB max
		try {
			URL url = new URL(inUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10 * 1000); // 10 secs
			conn.setConnectTimeout(15 * 1000); // 15 secs
			conn.setRequestMethod("GET");
			conn.setDoInput(true);

			conn.connect();
			int response = conn.getResponseCode();
			if (response != 200) {
				throw new IOException("Failed to download, got response code: "
						+ response + ", url: " + inUrl);
			}
			is = conn.getInputStream();

			String content = readIt(is, maxLen);
			return content;
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}

	private String readIt(InputStream is, int maxLen) throws IOException {
		Reader reader = new InputStreamReader(is, "UTF-8");
		StringBuffer sb = new StringBuffer();
		final char[] buffer = new char[2049];
		int read;
		while ((read = reader.read(buffer, 0, buffer.length)) != -1) {
			sb.append(buffer, 0, read);
		}
		return sb.toString();
	}

}
