package nz.net.kallisti.android.hashigozake;

import nz.net.kallisti.android.hashigozake.gcm.ServerUtils;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.google.android.gcm.GCMBaseIntentService;

/**
 * @author robin
 */
public class GCMIntentService extends GCMBaseIntentService {

	public static final String UPDATE_GCM_KEY = "update";
	public static final String LATEST_GCM_KEY = "latest";
	public static final String SENDER_ID = "422928883446";
	@SuppressWarnings("unused")
	private static final String DEBUG_TAG = "GCMIntentService";

	public GCMIntentService() {
		super(SENDER_ID);
	}
	
	@Override
	protected void onError(Context context, String errorId) {

	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		Bundle extras = intent.getExtras();
		String update = extras.getString(UPDATE_GCM_KEY);
		String latest = extras.getString(LATEST_GCM_KEY);
		if (update != null) {
			// We set the update flag in this to tell the app to do a refresh
			// when it next starts.
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
			Editor editor = prefs.edit();
			editor.putBoolean(UPDATE_GCM_KEY, true);
			if (latest != null) {
				editor.putString(LATEST_GCM_KEY, latest);
			}
			editor.apply();
			NotificationHandler.sendUpdateNotification(context, false);
		}
	}

	@Override
	protected void onRegistered(Context context, String regId) {
		ServerUtils.register(context,regId);
	}

	@Override
	protected void onUnregistered(Context context, String regId) {
		ServerUtils.unregister(context,regId);
	}
	
	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		return super.onRecoverableError(context, errorId);
	}

}
