package nz.net.kallisti.android.hashigozake;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gcm.GCMRegistrar;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.HashigoProductsListener;
import nz.net.kallisti.android.hashigozake.data.HashigoProductsRequest;
import nz.net.kallisti.android.hashigozake.data.PiesListener;
import nz.net.kallisti.android.hashigozake.data.PiesRequest;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProducts;
import nz.net.kallisti.android.hashigozake.data.pies.Pies;
import nz.net.kallisti.android.hashigozake.ui.AboutDialogFragment;

public class MainActivity extends AppCompatActivity implements BeerListFragment.OnFragmentInteractionListener,
        BottleListFragment.OnFragmentInteractionListener, PieListFragment.OnFragmentInteractionListener, ProductSelectedListener{
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String TAB = "nz.net.kallisti.android.hashigozake.tab";
    private static final String PRODUCTS = "nz.net.kallisti.android.hashigozake.products";
    private static final String PIES = "nz.net.kallisti.android.hashigozake.pies";

    @Bind(R.id.layout) View layout;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.view_pager) ViewPager viewPager;
    @Bind(R.id.tab_layout) TabLayout tabLayout;
    private RequestQueue requestQueue;
    private MainPagerAdapter adapter;
    private HashMap<Integer, HashigoProducts> productsCache = new HashMap<>();
    private HashMap<Integer, Pies> piesCache = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        adapter = new MainPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        requestQueue = ((App)getApplication()).getRequestQueue();

        setupGCM();

        if (savedInstanceState != null) {
            viewPager.setCurrentItem(savedInstanceState.getInt(TAB));
            productsCache = (HashMap<Integer, HashigoProducts>) savedInstanceState.getSerializable(PRODUCTS);
            piesCache = (HashMap<Integer, Pies>) savedInstanceState.getSerializable(PIES);
        }

        boolean forceUpdate = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(GCMIntentService.UPDATE_GCM_KEY, false);
        if (forceUpdate) {
            viewPager.setCurrentItem(0);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TAB, viewPager.getCurrentItem());
        outState.putSerializable(PRODUCTS, productsCache);
        outState.putSerializable(PIES, piesCache);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                return true;
            case R.id.action_about:
                DialogFragment about = new AboutDialogFragment();
                about.show(getSupportFragmentManager(), "about_dialogue");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onProductSelected(HashigoProduct product) {
        Intent intent = new Intent(this, ProductActivity.class);
        intent.putExtra(ProductActivity.TITLE, "Beer");
        intent.putExtra(ProductActivity.PRODUCT, product);
        startActivity(intent);
    }

    @Override
    public void refreshBeerList(HashigoProductsListener listener, boolean force) {
        refreshProductsForFragment(0, getString(R.string.beer_xml_url), listener, force);
    }

    @Override
    public void refreshBottleList(HashigoProductsListener listener, boolean force) {
        refreshProductsForFragment(1, getString(R.string.bottle_xml_url), listener, force);
    }

    @Override
    public void refreshPieList(PiesListener listener, boolean force) {
        refreshPiesForFragment(2, getString(R.string.pie_xml_url), listener, force);
    }

    private void refreshProductsForFragment(final int fragmentPosition, final String url,
                                            final HashigoProductsListener listener, final boolean force) {
        HashigoProducts products = productsCache.get(fragmentPosition);
        if(products != null) {
            listener.onProducts(products);
            if (!force) {
                // Show what we have in cache if we have it, then continue onwards if we need to update
                listener.onProductsResponse();
                return;
            }
        }
        listener.onProductsRequested();
        HashigoProductsRequest req = new HashigoProductsRequest(url, new Response.Listener<HashigoProducts>() {
            @Override
            public void onResponse(HashigoProducts products) {
                productsCache.put(fragmentPosition, products);
                listener.onProducts(products);
                listener.onProductsResponse();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onProductsError();
                onVolleyError(error);
            }
        });
        req.setTag(TAG);
        requestQueue.add(req);
    }

    private void refreshPiesForFragment(final int fragmentPosition, final String url,
                                        final PiesListener listener, final boolean force) {
        Pies pies = piesCache.get(fragmentPosition);
        if (pies != null) {
            listener.onPies(pies);
            if (!force) {
                // Show what we have in cache if we have it, then continue onwards if we need to update
                listener.onPiesResponse();
                return;
            }
        }
        listener.onPiesRequested();
        PiesRequest req = new PiesRequest(url, new Response.Listener<Pies>() {
            @Override
            public void onResponse(Pies pies) {
                piesCache.put(fragmentPosition, pies);
                listener.onPies(pies);
                listener.onPiesResponse();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onPiesError();
                onVolleyError(error);
            }
        });
        req.setTag(TAG);
        requestQueue.add(req);
    }

    private void onVolleyError(VolleyError error) {
        int message;
        if(error instanceof NoConnectionError) {
            message = R.string.error_network_unavailable;
        } else if (error instanceof NetworkError) {
            message = R.string.error_io_error_loading;
        } else {
            message = R.string.error_unknown_error_loading;
        }
        Snackbar.make(layout, message, Snackbar.LENGTH_LONG).show();
    }

    private void setupGCM() {
        try {
            GCMRegistrar.checkDevice(this);
            // GCMRegistrar.checkManifest(this); // TODO Remove on publish
            final String regId = GCMRegistrar.getRegistrationId(this);
            if (regId.equals("")) {
                GCMRegistrar.register(this, GCMIntentService.SENDER_ID);
            }
        } catch (UnsupportedOperationException e) {
            // this means the device can't be registered. That's not the
            // end of the world, so we ignore it.
            Log.v(TAG, "Device not able to register", e);
        }
    }
}
