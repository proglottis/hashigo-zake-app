package nz.net.kallisti.android.hashigozake;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;

/**
 * Created by james on 14/09/15.
 */
public interface ProductSelectedListener {
    void onProductSelected(HashigoProduct product);
}
