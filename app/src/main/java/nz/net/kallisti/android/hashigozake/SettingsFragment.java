package nz.net.kallisti.android.hashigozake;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Created by james on 13/09/15.
 */
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }
}
