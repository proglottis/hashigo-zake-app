/*	Copyright (C) 2012 robin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package nz.net.kallisti.android.hashigozake;

import nz.net.kallisti.android.hashigozake.exceptions.UnknownLocationException;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

/**
 * This is used to check to see if we are near enough to Hashigo Zake for it
 * to be worthwhile sending an update notification.
 * 
 * @author robin
 */
public class LocationChecker {

	private static final String DEBUG_TAG = "LocationChecker";

	public static boolean closeEnough(Context context) throws UnknownLocationException {
		LocationManager locMan = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
		Location lastKnown = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if (lastKnown == null)
			throw new UnknownLocationException();
		// We're going to overwrite this with the coordinates for hz
		// -41.291710, 174.779557
		Location hzLoc = new Location(LocationManager.NETWORK_PROVIDER);
		hzLoc.setLatitude(-41.291710);
		hzLoc.setLongitude(174.779557);
		float dist = lastKnown.distanceTo(hzLoc);
		Log.i(DEBUG_TAG, "Distance from hz: "+dist);
		// 10km
		return dist <= 10000;
	}
	
}
