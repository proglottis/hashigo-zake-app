/*	Copyright (C) 2013 Robin Sheat

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nz.net.kallisti.android.hashigozake.data.pies;

import java.io.IOException;
import java.io.StringReader;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoParseException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

/**
 * This takes an XML file describing the pies and turns it into a {@link Pies}
 * object.
 * 
 * @author robin
 */
public class PieXMLParser {

	@SuppressWarnings("unused")
	private static final String DEBUG_TAG = "PieXMLParser";
	public static String ns = null;

	public Pies parseXML(String xml) throws HashigoParseException {
		XmlPullParser parser = Xml.newPullParser();
		try {
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(new StringReader(xml));
			parser.nextTag();
			return readFeed(parser);
		} catch (XmlPullParserException e) {
			throw new HashigoParseException(HashigoParseException.ERROR_PARSE,
					e);
		} catch (IOException e) {
			throw new HashigoParseException(HashigoParseException.ERROR_IO, e);
		}
	}

	private Pies readFeed(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		Pies pies = new Pies();
		parser.require(XmlPullParser.START_TAG, ns, "Pies");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("Pie")) {
//				loadPies(pies, parser);
				Pie pie = loadPie(parser);
				pies.addPie(pie);
			} else {
				skip(parser);
			}
		}
		return pies;
	}

	private Pie loadPie(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String pieName = "";
		String available = "No";
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			try {
				if (name.equals("Name")) {
					pieName = readText(parser);
				} else if (name.equals("Available")) {
					available = readText(parser);
				} else {
					skip(parser);
				}
			} catch (NullPointerException e) {
			}
		}
		return new Pie(pieName, "Yes".equals(available));
	}

	private String readText(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}


	
	private void skip(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException(
					"Attempting to skip from something that's not a start tag");
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}
}
