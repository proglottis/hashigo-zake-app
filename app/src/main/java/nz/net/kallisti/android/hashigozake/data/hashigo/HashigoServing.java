package nz.net.kallisti.android.hashigozake.data.hashigo;

import java.io.Serializable;

/**
 * This describes a serving by volume and cost
 * 
 * @author robin
 */
public class HashigoServing implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5468605474604198486L;

	private int volume; // in mls

	private int price; // in cents

	/**
	 * Create a new serving with the provided volume and price
	 * 
	 * @param volume
	 *            the volume of the serving (in ml)
	 * @param price
	 *            the price of the serving (in cents)
	 */
	public HashigoServing(int volume, int price) {
		this.volume = volume;
		this.price = price;
	}

	/**
	 * Gets the volume of this serving
	 * 
	 * @return the volume in ml
	 */
	public int getVolume() {
		return volume;
	}

	/**
	 * Gets the price of this serving
	 * 
	 * @return the price of this serving, in cents
	 */
	public int getPrice(boolean isSobaMember) {
		if(isSobaMember) {
			return (int) (price * 0.9);
		}
		
		return price;
	}

}
