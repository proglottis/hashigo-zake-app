/*	Copyright (C) 2013 Robin Sheat

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nz.net.kallisti.android.hashigozake.data.pies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This represents a collection of pies
 * 
 * @author robin
 */
public class Pies implements Serializable {

	private static final long serialVersionUID = 5566672574972444206L;
	List<Pie> pies = new ArrayList<Pie>();

	/**
	 * Gives you all the pies that are currently available
	 * 
	 * @return available pies
	 */
	public List<Pie> getAvailablePies() {
		ArrayList<Pie> res = new ArrayList<Pie>();
		for (Pie pie : pies) {
			if (pie.isAvailable())
				res.add(pie);
		}
		return res;
	}

	/**
	 * Adds a new pie
	 * 
	 * @param pie
	 *            the new pie
	 */
	public void addPie(Pie pie) {
		pies.add(pie);
	}
}
