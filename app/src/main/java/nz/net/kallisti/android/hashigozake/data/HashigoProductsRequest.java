package nz.net.kallisti.android.hashigozake.data;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoParseException;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProducts;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoXMLParser;

public class HashigoProductsRequest extends Request<HashigoProducts> {
    private final Response.Listener<HashigoProducts> listener;

    public HashigoProductsRequest(String url, Response.Listener<HashigoProducts> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.listener = listener;
    }

    @Override
    protected Response<HashigoProducts> parseNetworkResponse(NetworkResponse response) {
        try {
            HashigoXMLParser parser = new HashigoXMLParser();
            //TODO: the charset should be parsed from the response but the server doesn't seem to respond correctly
            String data = new String(response.data, "UTF-8");
            HashigoProducts products = parser.parseXML(data);
            return Response.success(products, HttpHeaderParser.parseCacheHeaders(response));
        } catch (HashigoParseException e) {
            return Response.error(new ParseError(e));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(HashigoProducts response) {
        listener.onResponse(response);
    }
}
