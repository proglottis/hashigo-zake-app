/*	Copyright (C) 2012 robin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nz.net.kallisti.android.hashigozake;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import java.util.Timer;
import java.util.TimerTask;

import nz.net.kallisti.android.hashigozake.exceptions.UnknownLocationException;

/**
 * <p>
 * Checks to see if notifications need to be sent, and if so, sends them.
 * </p>
 * 
 * @author robin
 */
public class NotificationHandler {

	private static final String LAST_NOTIFIED = "last_notified";

	/**
	 * Call this when something might have been updated, and the latest update
	 * is stored in {@link GCMIntentService.LATEST_GCM_KEY}. This will determine
	 * if a notification needs to be sent, and if so, will send it.
	 * 
	 * @param context
	 *            the application context
	 * @param tryingAgain
	 *            indicates that this is a retry. Set this to <code>false</code>
	 *            .
	 */
	public static void sendUpdateNotification(final Context context,
			boolean tryingAgain) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		String latest = prefs.getString(GCMIntentService.LATEST_GCM_KEY, null);
		String lastNotif = prefs.getString(LAST_NOTIFIED, "");
		if (lastNotif == null || (!lastNotif.equals(latest) && latest != null)) {
			String notifTitle = context.getResources().getString(
					R.string.notification_title);
			String notifBody = context.getResources().getString(
					R.string.notification_text);
			notifBody = String.format(notifBody, latest);
			boolean geo = prefs.getBoolean(
					SettingsActivity.PREF_NOTIFY_GEOLOCATE, true);
			boolean close = false;
			if (geo) {
				try {
					close = LocationChecker.closeEnough(context);
				} catch (UnknownLocationException e) {
					Timer timer = new Timer();
					// try again in 10 minutes - hopefully we know where we are
					// then. E.g. people who have just got off an aeroplane
					// may not know where they are.
					timer.schedule(new TimerTask() {

						@Override
						public void run() {
							sendUpdateNotification(context, true);
						}

					}, 600000);
					return;
				}
			} else {
				close = true;
			}
			if (close) {
				boolean notifyDevice = prefs.getBoolean(
						SettingsActivity.PREF_NOTIFY_DEVICE, true);
				if (notifyDevice)
					updateNotification(context, notifTitle, notifBody);
			}
		}
		Editor editor = prefs.edit();
		editor.putString(LAST_NOTIFIED, latest);
		editor.apply();
	}

	/**
	 * Internal method to send the actual device notification.
	 */
	private static void updateNotification(Context context, String notifTitle,
			String notifBody) {
		Intent resultIntent = new Intent(context, MainActivity.class);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);
		NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(
				context).setSmallIcon(R.drawable.ic_notification)
				.setContentTitle(notifTitle).setContentText(notifBody)
				.setContentIntent(resultPendingIntent);
		NotificationManager notifManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification n = notifBuilder.build();
		n.flags = n.flags | Notification.FLAG_AUTO_CANCEL;
		notifManager.notify(0, n);
	}

}
