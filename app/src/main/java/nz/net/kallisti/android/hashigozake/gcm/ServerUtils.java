package nz.net.kallisti.android.hashigozake.gcm;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import nz.net.kallisti.android.hashigozake.R;
import android.content.Context;

import com.google.android.gcm.GCMRegistrar;

/**
 * <p>
 * This is a collection of static methods that handle communicating to the
 * server to register/unregister this application as something that can handle
 * GCM messages.
 * </p>
 * 
 * <p>
 * This code is mostly based on the GCM code sample in the Android SDK.
 * </p>
 * 
 * @author robin
 */
public class ServerUtils {

	private static final int MAX_ATTEMPTS = 5;
	private static final int BACKOFF_MILLI_SECONDS = 2000;
	private static final Random random = new Random();
	@SuppressWarnings("unused")
	private static final String DEBUG_TAG = "ServerUtils";

	/**
	 * Register this account/device pair within the server.
	 * 
	 * @return whether the registration succeeded or not.
	 */
	public static boolean register(Context context, final String regId) {
		String serverUrl = context.getString(R.string.gcm_server_register);
		Map<String, String> params = new HashMap<String, String>();
		params.put("regId", regId);
		long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
		// Once GCM returns a registration id, we need to register it in the
		// demo server. As the server might be down, we will retry it a couple
		// times.
		for (int i = 1; i <= MAX_ATTEMPTS; i++) {
			try {
				get(serverUrl, params);
				GCMRegistrar.setRegisteredOnServer(context, true);
				return true;
			} catch (IOException e) {
				// TODO Here we are simplifying and retrying on any error; in a real
				// application, it should retry only on unrecoverable errors
				// (like HTTP error code 503).
				if (i == MAX_ATTEMPTS) {
					break;
				}
				try {
					Thread.sleep(backoff);
				} catch (InterruptedException e1) {
					// Activity finished before we complete - exit.
					Thread.currentThread().interrupt();
					return false;
				}
				// increase backoff exponentially
				backoff *= 2;
			}
		}
		return false;
	}

    /**
     * Unregister this account/device pair within the server.
     */
    public static void unregister(final Context context, final String regId) {
		String serverUrl = context.getString(R.string.gcm_server_unregister);
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        try {
            get(serverUrl, params);
            GCMRegistrar.setRegisteredOnServer(context, false);
        } catch (IOException e) {
            // At this point the device is unregistered from GCM, but still
            // registered in the server.
            // We could try to unregister again, but it is not necessary:
            // if the server tries to send a message to the device, it will get
            // a "NotRegistered" error message and should unregister the device.
        }
    }
	/**
	 * Issue a GET request to the server.
	 * 
	 * @param endpoint
	 *            POST address.
	 * @param params
	 *            request parameters.
	 * 
	 * @throws IOException
	 *             propagated from GET.
	 */
	private static void get(String endpoint, Map<String, String> params)
			throws IOException {
		StringBuilder argBuilder = new StringBuilder();
		Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
		// constructs the GET body using the parameters
		while (iterator.hasNext()) {
			Entry<String, String> param = iterator.next();
			argBuilder.append(param.getKey()).append('=')
					.append(param.getValue());
			if (iterator.hasNext()) {
				argBuilder.append('&');
			}
		}
		URL url;
		String args = argBuilder.toString();
		try {
			url = new URL(endpoint + "?" + args);
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("invalid url: " + endpoint);
		}
		HttpURLConnection conn = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setRequestMethod("GET");
			InputStream in = new BufferedInputStream(conn.getInputStream());
			in.read(); // We don't care about the content
			// handle the response
			int status = conn.getResponseCode();
			if (status != 200) {
				throw new IOException("Post failed with error code " + status);
			}
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}
}
