package nz.net.kallisti.android.hashigozake.data.hashigo;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct.PouringStatus;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

/**
 * This processes the XML file, understanding the hashigo data format.
 * 
 * @author robin
 */
public class HashigoXMLParser {

	@SuppressWarnings("unused")
	private static final String DEBUG_TAG = "HashigoXMLParser";
	public static String ns = null;

	public HashigoProducts parseXML(String xml) throws HashigoParseException {
		XmlPullParser parser = Xml.newPullParser();
		try {
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(new StringReader(xml));
			parser.nextTag();
			return readFeed(parser);
		} catch (XmlPullParserException e) {
			throw new HashigoParseException(HashigoParseException.ERROR_PARSE,
					e);
		} catch (IOException e) {
			throw new HashigoParseException(HashigoParseException.ERROR_IO, e);
		}
	}

	private HashigoProducts readFeed(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		HashigoProducts products = new HashigoProducts();
		parser.require(XmlPullParser.START_TAG, ns, "Products");
		String timestamp = parser.getAttributeValue(ns, "Timestamp");
		products.setTimestamp(timestamp);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("Beers")) {
				loadBeers(products, parser);
			} else if (name.equals("LastChange")) {
				lastChanged(products, parser);
			} else {
				skip(parser);
			}
		}
		return products;
	}

	private void loadBeers(HashigoProducts products, XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "Beers");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("Product") || name.equals("Beer")) {
				HashigoProduct product = loadProduct(parser);
				products.addProduct(product);
			} else {
				skip(parser);
			}
		}
	}

	private HashigoProduct loadProduct(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		HashigoProduct product = new HashigoProduct();
		String volume = null;
		String price = null;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			try {
				if (name.equals("Name")) {
					product.setName(readText(parser));
				} else if (name.equals("Volume")) {
					volume = readText(parser);
				} else if (name.equals("Price")) {
					price = readText(parser);
				} else if (name.equals("Image")) {
					product.setImage(readText(parser));
				} else if (name.equals("ABV")) {
					String text = readText(parser);
					int pos;
					if (text != null && (pos = text.indexOf('%')) >= 0) {
						product.setAbv(Float.parseFloat(text.substring(0, pos)));
					}
				} else if (name.equals("Handpump")) {
					product.setHandpump("Yes".equals(readText(parser)) ? true
							: false);
				} else if (name.equals("Country")) {
					product.setCountry(readText(parser));
				} else if (name.equals("Brewery")) {
					product.setBrewery(readText(parser));
				} else if (name.equals("Description") || name.equals("Style")) {
					product.setDescription(readText(parser));
				} else if (name.equals("KegVolume")) {
					product.setKegVolume(Float.parseFloat(readText(parser)));
				} else if (name.equals("Pouring")) {
					product.setPouringStatus(PouringStatus.valueOf(readText(
							parser).toUpperCase()));
				} else if (name.equals("Ratebeer")) {
					product.setRatebeerURL(readText(parser));
				} else {
					skip(parser);
				}
			} catch (NumberFormatException e) {
			} catch (NullPointerException e) {
			}
		}
		List<HashigoServing> servings = createServings(volume, price);
		for (HashigoServing s : servings) {
			product.addServing(s);
		}
		return product;
	}

	private List<HashigoServing> createServings(String volume, String price) {
		List<HashigoServing> result = new ArrayList<HashigoServing>();
		if (volume == null || price == null)
			return result;
		String[] vols = volume.split("/");
		String[] prices = price.split("/");
		if (vols.length != prices.length)
			return result;
		for (int i = 0; i < vols.length; i++) {
			String v = vols[i];
			String p = prices[i];
			// Get the number from the volume part by stripping any non-digits.
			// I hope they don't start serving litres.
			v = v.replaceAll("\\D+", "");
			// Remove the $ from the price
			p = p.replaceAll("[^\\d.]+", "");
			// Now convert it into cents
			String[] priceParts = p.split("\\.");
			if (priceParts.length != 1 && priceParts.length != 2)
				return result;
			try {
				if (priceParts.length == 1) {
					p = priceParts[0] + "00";
				} else {
					p = priceParts[0] + priceParts[1]
							+ (priceParts[1].length() == 1 ? "0" : "");
				}
				result.add(new HashigoServing(Integer.parseInt(v), Integer
						.parseInt(p)));
			} catch (NumberFormatException e) {
				// Do nothing
			}
		}
		return result;
	}
	
	private void lastChanged(HashigoProducts products, XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "LastChange");
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();
			if (name.equals("On")) {
				products.setLastOn(readText(parser));
			} else {
				skip(parser);
			}
		}
	}

	private String readText(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}

	private void skip(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException(
					"Attempting to skip from something that's not a start tag");
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}

}
