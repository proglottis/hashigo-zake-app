/*	Copyright (C) 2012 Robin Sheat

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package nz.net.kallisti.android.hashigozake.ui;

import nz.net.kallisti.android.hashigozake.R;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AboutDialogFragment extends DialogFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.about_dialogue, container, false);
		TextView tv = (TextView) v.findViewById(R.id.about_textview);
		Spanned text = Html.fromHtml(getString(R.string.about_content));
		tv.setText(text);
		tv.setMovementMethod(LinkMovementMethod.getInstance());
		getDialog().setTitle(R.string.about_title);
		return v;
	}
	
}
