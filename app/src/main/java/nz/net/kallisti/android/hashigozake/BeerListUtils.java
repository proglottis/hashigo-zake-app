package nz.net.kallisti.android.hashigozake;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import java.text.DecimalFormat;
import java.util.Calendar;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;

/**
 * General utility functions
 * 
 * @author robin
 */
public class BeerListUtils {

	public static CharSequence formatPrice(int price) {
		DecimalFormat priceFormatter = new DecimalFormat("$#0.00");
		return priceFormatter.format(price / 100.0);
	}

	public static boolean isSobaDay() {
		Calendar cal = Calendar.getInstance();
		int dow = cal.get(Calendar.DAY_OF_WEEK);
		// easy case
		if (dow == Calendar.MONDAY || dow == Calendar.TUESDAY || dow == Calendar.WEDNESDAY) {
			return true;
		}
		// hard cases
		final int crossover_time = 8; // when do we consider it a new day?
		if (dow == Calendar.SUNDAY && cal.get(Calendar.HOUR_OF_DAY) > crossover_time) {
			return true;
		}
		if (dow == Calendar.THURSDAY && cal.get(Calendar.HOUR_OF_DAY) < crossover_time) {
			return true;
		}
		return false;
	}
	
	public static boolean doesSobaDiscountApply(Context context) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		boolean sobaMember = prefs.getBoolean(
                SettingsActivity.PREF_SOBA_MEMBER, false);
		
		return isSobaDay() && sobaMember;
	}

	public static Uri getRatebeerUri(Context context, HashigoProduct product) {
        if (product.getRatebeerURL() != null && !product.getRatebeerURL().isEmpty()) {
            return Uri.parse(product.getRatebeerURL());
        }
        return Uri.parse(context.getString(R.string.ratebeer_search_url_base))
                .buildUpon()
                .appendQueryParameter("BeerName", product.toString())
                .build();
	}

    public static Uri getUntappdUri(Context context, HashigoProduct product) {
        return Uri.parse(context.getString(R.string.untappd_search_url_base))
                .buildUpon()
                .appendQueryParameter("q", product.toString())
                .build();
    }
}
