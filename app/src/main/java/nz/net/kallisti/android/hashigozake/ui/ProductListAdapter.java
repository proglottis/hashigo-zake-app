package nz.net.kallisti.android.hashigozake.ui;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.peterkuterna.android.apps.pinnedheader.PinnedListView;
import net.peterkuterna.android.apps.pinnedheader.PinnedListView.PinnedHeaderAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.BeerListUtils;
import nz.net.kallisti.android.hashigozake.R;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct.PouringStatus;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProducts;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoServing;

/**
 * This generates a view showing a product, designed to go in a listview or
 * similar.
 *
 * The product views are ordered by pouring status.
 *
 * @author robin
 */
public class ProductListAdapter extends BaseAdapter implements PinnedHeaderAdapter, OnScrollListener {

    @SuppressWarnings("unused")
    private static final String TAG = "ProductListAdapter";
    protected LayoutInflater inflater;
    protected Context context;
    protected HashigoProducts products = new HashigoProducts();
    private List<HashigoProduct> data = Collections.emptyList();
    private Map<HashigoProduct, String> headers = new HashMap<>();
    private Map<HashigoProduct, String> headersFirst = new HashMap<>();

    public ProductListAdapter(Context context) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void updateAll(HashigoProducts products) {
        this.products = products;
        headers.clear();
        headersFirst.clear();
        data = getProductsToShow(products);
        notifyDataSetChanged();
    }

    /**
     * Produce a list of products that will be shown in the list view. As a
     * side-effect, this generates the header values.
     *
     * @param products all the products loaded.
     * @return a list of the products to show
     */
    protected List<HashigoProduct> getProductsToShow(HashigoProducts products) {
        ArrayList<HashigoProduct> data = new ArrayList<>();
        List<HashigoProduct> prods = products
                .getProductsWhere(PouringStatus.NEWEST);
        setHeaderFor(prods, context.getString(R.string.pouring_newest_header));
        data.addAll(prods);
        prods = products.getProductsWhere(PouringStatus.NOW);
        setHeaderFor(prods, context.getString(R.string.pouring_now_header));
        data.addAll(prods);
        prods = products.getProductsWhere(PouringStatus.NEXT);
        setHeaderFor(prods, context.getString(R.string.pouring_next_header));
        data.addAll(prods);
        prods = products.getProductsWhere(PouringStatus.NO);
        setHeaderFor(prods, context.getString(R.string.pouring_no_header));
        data.addAll(prods);
        return data;
    }

    private void setHeaderFor(List<HashigoProduct> prod, String header) {
        if (prod.isEmpty()) {
            return;
        }
        headersFirst.put(prod.get(0), header);
        for (HashigoProduct p : prod) {
            headers.put(p, header);
        }
    }

    private String getHeaderFor(HashigoProduct prod, boolean firstOnly) {
        if (firstOnly) {
            return headersFirst.get(prod);
        }
        return headers.get(prod);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ProductViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.beer_list_item, parent, false);
            viewHolder = new ProductViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ProductViewHolder) convertView.getTag();
        }
        HashigoProduct product = data.get(position);
        viewHolder.populateView(product);
        return convertView;
    }

    /**
     * Does a SOBA discount apply for this particular view?
     */
    boolean sobaDiscountApplies() {
        return true;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getPinnedHeaderState(int position) {
        // this corrects for which view causes the push up to happen
        int actualPos = position > 0 ? position + 1 : position;
        HashigoProduct prod = data.get(actualPos);
        if (getHeaderFor(prod, true) == null) {
            return PINNED_HEADER_VISIBLE;
        }
        return PINNED_HEADER_PUSHED_UP;
    }

    @Override
    public void configurePinnedHeader(View header, int position, int alpha) {
        TextView text = (TextView) header;
        HashigoProduct prod = data.get(position);
        text.setText(getHeaderFor(prod, false));
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        if (view instanceof PinnedListView) {
            ((PinnedListView) view).configureHeaderView(firstVisibleItem);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    class ProductViewHolder {
        View itemView;

        @Nullable
        @Bind(R.id.beer_list_item_header)
        TextView headerText;

        @Bind(R.id.beer_list_item_name)
        TextView name;

        @Bind(R.id.handpump)
        TextView handpump;

        @Bind(R.id.beer_list_item_description)
        TextView description;

        @Bind(R.id.detail_expanded_prices_container)
        ViewGroup pricesContainer;

        public ProductViewHolder(View itemView) {
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void populateView(HashigoProduct product) {
            fillInPrices(product.getServings());

            name.setText(product.getName());
            if (product.isHandpump()) {
                handpump.setVisibility(View.VISIBLE);
            } else {
                handpump.setVisibility(View.GONE);
            }
            String descriptionText = product.getBrewery();
            if (product.getAbv() > 0f) {
                if (!descriptionText.isEmpty()) {
                    descriptionText += ", ";
                }
                descriptionText += itemView.getResources().getString(R.string.percentage, product.getAbv());
            }
            if (!product.getDescription().isEmpty()) {
                if (!descriptionText.isEmpty()) {
                    descriptionText += ", ";
                }
                descriptionText += product.getDescription();
            }
            description.setText(descriptionText);


            // Set the header
            if (headerText != null) {
                String text;
                if ((text = getHeaderFor(product, true)) == null) {
                    headerText.setVisibility(View.GONE);
                } else {
                    headerText.setVisibility(View.VISIBLE);
                    headerText.setText(text);
                }
            }
        }

        private void fillInPrices(ArrayList<HashigoServing> servings) {
            pricesContainer.removeAllViews();
            if (servings == null) {
                return;
            }
            for (HashigoServing serving : servings) {
                View servingView = inflater.inflate(R.layout.beer_detail_serving, pricesContainer, false);
                TextView price = (TextView) servingView.findViewById(R.id.beer_detail_serving_price);
                TextView vol = (TextView) servingView.findViewById(R.id.beer_detail_serving_volume);
                price.setText(BeerListUtils.formatPrice(serving
                        .getPrice(sobaDiscountApplies()
                                && BeerListUtils.doesSobaDiscountApply(itemView.getContext()))));
                vol.setText(itemView.getResources().getString(R.string.millilitres, serving.getVolume()));
                pricesContainer.addView(servingView);
            }
        }
    }
}
