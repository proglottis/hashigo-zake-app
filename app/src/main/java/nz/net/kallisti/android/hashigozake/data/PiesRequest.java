package nz.net.kallisti.android.hashigozake.data;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoParseException;
import nz.net.kallisti.android.hashigozake.data.pies.PieXMLParser;
import nz.net.kallisti.android.hashigozake.data.pies.Pies;

/**
 * Created by james on 9/09/15.
 */
public class PiesRequest extends Request<Pies> {
    private final Response.Listener<Pies> listener;

    public PiesRequest(String url, Response.Listener<Pies> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.listener = listener;
    }

    @Override
    protected Response<Pies> parseNetworkResponse(NetworkResponse response) {
        try {
            PieXMLParser parser = new PieXMLParser();
            //TODO: the charset should be parsed from the response but the server doesn't seem to respond correctly
            String data = new String(response.data, "UTF-8");
            Pies pies = parser.parseXML(data);
            return Response.success(pies, HttpHeaderParser.parseCacheHeaders(response));
        } catch (HashigoParseException e) {
            return Response.error(new ParseError(e));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(Pies response) {
        listener.onResponse(response);
    }
}
