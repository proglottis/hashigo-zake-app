package nz.net.kallisti.android.hashigozake.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.R;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct.PouringStatus;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProducts;

/**
 * Handles the display of the bottle list, which is slightly different to the
 * display of the beer list.
 * 
 * @author robin
 */
public class BottleListAdapter extends ProductListAdapter implements ExpandableListAdapter {

    public enum SortBy {
		BREWERY, STYLE, ALPHABETICAL
	}

	private static final String TAG = "BottleListAdapter";

	private SortBy currentSort = SortBy.BREWERY; // TODO restore from save
	private SortedData sortedData;

	private static class SortedData {
		List<String> headers;

		List<String> subtitles;

		List<List<HashigoProduct>> products;
	}

    public BottleListAdapter(Context context) {
        super(context);
    }

    @Override
    public void updateAll(HashigoProducts products) {
        super.updateAll(products);
        sortedData = sortData(products, currentSort);
    }

    public void updateAll(HashigoProducts products, SortBy currentSort) {
        this.currentSort = currentSort;
        updateAll(products);
    }

    private SortedData sortData(HashigoProducts allProducts, SortBy currentSort) {
		List<HashigoProduct> products = getProductsToShow(allProducts);
		TreeMap<String, TreeSet<HashigoProduct>> sortedProducts = new TreeMap<>();
		Comparator<HashigoProduct> comparator = new Comparator<HashigoProduct>() {
			@Override
			public int compare(HashigoProduct lhs, HashigoProduct rhs) {
				String nameL = lhs.toString();
				String nameR = rhs.toString();
				return nameL.compareTo(nameR);
			}
		};
		// First put the products into an ordered structure so we can easily
		// pull them out in the right order later.
		for (HashigoProduct hp : products) {
			String header;
			switch (currentSort) {
			case BREWERY:
				header = hp.getBrewery();
				break;
			case STYLE:
				header = hp.getDescription();
				break;
			case ALPHABETICAL:
				String name = hp.getName();
				if (name == null || name.length() < 1)
					header = context.getString(R.string.unknown_header);
				else
					header = hp.getName().substring(0, 1);
				break;
			default:
				throw new IllegalArgumentException(
						"Unknown sort order provided: " + currentSort);
			}
			if (header == null || header.length() < 1)
				header = context.getString(R.string.unknown_header);
			TreeSet<HashigoProduct> set = sortedProducts.get(header);
			if (set == null) {
				set = new TreeSet<>(comparator);
				sortedProducts.put(header, set);
			}
			set.add(hp);
		}
		// Now put the items into the sorted data structure
		SortedData data = new SortedData();
		data.headers = new ArrayList<>();
		data.subtitles = new ArrayList<>();
		data.products = new ArrayList<>();
		for (String header : sortedProducts.keySet()) {
			data.headers.add(header);
			ArrayList<HashigoProduct> prodList = new ArrayList<>();
			data.products.add(prodList);
			String country = null;
			for (HashigoProduct p : sortedProducts.get(header)) {
				String c = p.getCountry();
				if (c != null && c.length() > 0)
					country = c;
				prodList.add(p);
			}
			switch (currentSort) {
			case BREWERY:
				data.subtitles.add(country);
				break;
			default:
				data.subtitles.add(null);
			}
		}
		return data;
	}

	/**
	 * Produce a list of products that will be shown in the list view.
	 * 
	 * @param products
	 *            all the products loaded.
	 * @return a list of the products to show
	 */
	protected List<HashigoProduct> getProductsToShow(HashigoProducts products) {
		ArrayList<HashigoProduct> data = new ArrayList<>();
		data.addAll(products.getProductsWhere(PouringStatus.NEWEST));
		data.addAll(products.getProductsWhere(PouringStatus.NOW));
		return data;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return sortedData.products.get(groupPosition).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// Only needs to be unique within the group
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		HashigoProduct product = sortedData.products.get(groupPosition).get(
				childPosition);
		ProductViewHolder viewHolder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.beer_list_item, parent, false);
			viewHolder = new ProductViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ProductViewHolder) convertView.getTag();
		}
		viewHolder.populateView(product);
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return sortedData.products.get(groupPosition).size();
	}

	@Override
	public long getCombinedChildId(long groupId, long childId) {
		// The ID is the location in the index if we were to flatten all the
		// lists, leaving a space for the header (hence the +1)
		return +(childId + 1);
	}

	@Override
	public long getCombinedGroupId(long groupId) {
		return (sortedData.products.get((int) groupId).size() + 1) * groupId;
	}

	@Override
	public String getGroup(int groupPosition) {
		return sortedData.headers.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
        if (sortedData == null) {
            return 0;
        }
		return sortedData.headers.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
        HeaderViewHolder viewHolder;
		if (convertView == null) {
            convertView = inflater.inflate(R.layout.bottle_list_header, parent, false);
            viewHolder = new HeaderViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (HeaderViewHolder) convertView.getTag();
        }
        viewHolder.populateView(sortedData.headers.get(groupPosition), sortedData.subtitles.get(groupPosition), isExpanded);
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
	}

	/**
	 * Sets the sorting method to use. This will notify listeners to update
	 * themselves.
	 * 
	 * @param sort
	 *            the new sort to use
	 */
	public void setCurrentSort(SortBy sort) {
		currentSort = sort;
		sortedData = sortData(products, currentSort);
		notifyDataSetChanged();
	}

	@Override
	boolean sobaDiscountApplies() {
		return false;
	}

    static class HeaderViewHolder {
        View itemView;
        @Bind(R.id.bottle_list_header_title) TextView text;
		@Bind(R.id.indicator) ImageView indicator;

        public HeaderViewHolder(View itemView) {
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);
        }

        public void populateView(String title, String subtitle, boolean isExpanded) {
            text.setText(title);
            if (isExpanded) {
                indicator.getDrawable().setState(new int[]{android.R.attr.state_expanded});
                text.setTextColor(itemView.getResources().getColor(R.color.red_500));
            } else {
                indicator.getDrawable().setState(new int[]{});
                text.setTextColor(itemView.getResources().getColor(android.R.color.primary_text_dark));
            }
        }
    }
}
