package nz.net.kallisti.android.hashigozake.data;

/**
 * Implementations of this can handle the data sent by the {@link TextHTTPLoader}
 * 
 * @author robin
 */
public interface Processor<T> {

	/**
	 * Indicates that there was an IO error while loading the text
	 */
	public static int ERR_IO = 0x1;
	public static int ERR_NO_NETWORK = 0x2;	

	/**
	 * This gives the data to the processor
	 * @param result the result of the process
	 */
	public void process(T result);
	
	/**
	 * An error occurred.
	 * @param errCode the error code
	 */
	public void error(int errCode);
	
}
