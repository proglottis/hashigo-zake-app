package nz.net.kallisti.android.hashigozake.data.hashigo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This represents an individual beer and its details
 * 
 * @author robin
 */
public class HashigoProduct implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2414225170180565272L;

	public enum PouringStatus {
		NO, NOW, NEXT, NEWEST
	};

	/**
	 * The name of the beer
	 */
	private String name;
	/**
	 * The servings that it's available in
	 */
	private ArrayList<HashigoServing> servings = new ArrayList<HashigoServing>();
	/**
	 * The URL of the image
	 */
	private String image;
	/**
	 * The alcohol percentage
	 */
	private float abv;
	/**
	 * Is it on handpump?
	 */
	private boolean isHandpump;
	/**
	 * The country it's from
	 */
	private String country;
	/**
	 * The brewery it's from
	 */
	private String brewery;
	/**
	 * A brief description
	 */
	private String description;
	/**
	 * The volume of the keg. This is usually unpopulated (i.e. is '0.000000')
	 */
	private float kegVolume;
	/**
	 * The current pouring status
	 */
	private PouringStatus pouringStatus;
	/**
	 * The URL to the ratebeer page for this
	 */
	private String ratebeerURL;

	/**
	 * Get the name of the beer
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of the beer
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the image URL
	 * 
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Set the image URL
	 * 
	 * @param image
	 *            the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Get the alcohol percentage
	 * 
	 * @return the abv
	 */
	public float getAbv() {
		return abv;
	}

	/**
	 * Set the alcohol percentage
	 * 
	 * @param abv
	 *            the abv to set
	 */
	public void setAbv(float abv) {
		this.abv = abv;
	}

	/**
	 * Is it on handpump?
	 * 
	 * @return <code>true</code> or <code>false</code>
	 */
	public boolean isHandpump() {
		return isHandpump;
	}

	/**
	 * Set if it's on handpump
	 * 
	 * @param isHandpump
	 *            <code>true</code> or <code>false</code>
	 */
	public void setHandpump(boolean isHandpump) {
		this.isHandpump = isHandpump;
	}

	/**
	 * Get the country it's from
	 * 
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Set the country it's from
	 * 
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Get the brewery it's from
	 * 
	 * @return the brewery
	 */
	public String getBrewery() {
		return brewery;
	}

	/**
	 * Set the brewery it's from
	 * 
	 * @param brewery
	 *            the brewery to set
	 */
	public void setBrewery(String brewery) {
		this.brewery = brewery;
	}

	/**
	 * Get the brief description
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set the brief description
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Get the keg volume. 0.000000 means that this is unset
	 * 
	 * @return the keg volume
	 */
	public float getKegVolume() {
		return kegVolume;
	}

	/**
	 * Set the keg volume
	 * 
	 * @param kegVolume
	 *            the keg volume to set
	 */
	public void setKegVolume(float kegVolume) {
		this.kegVolume = kegVolume;
	}

	/**
	 * Get the current pouring status
	 * 
	 * @return the pouringStatus
	 */
	public PouringStatus getPouringStatus() {
		return pouringStatus;
	}

	/**
	 * Set the current pouring status
	 * 
	 * @param pouringStatus
	 *            the pouringStatus to set
	 */
	public void setPouringStatus(PouringStatus pouringStatus) {
		this.pouringStatus = pouringStatus;
	}

	/**
	 * Get a list of the available servings
	 * 
	 * @return the servings
	 */
	public ArrayList<HashigoServing> getServings() {
		return servings;
	}

	/**
	 * Adds a serving
	 * 
	 * @param serving
	 */
	public void addServing(HashigoServing serving) {
		servings.add(serving);
	}

	/**
	 * Set whether this is the newest product
	 */
	protected void setNewest() {
		pouringStatus = PouringStatus.NEWEST;
	}

	/**
	 * Sets the URL to ratebeer for this product. If the URL isn't valid, then
	 * it isn't added.
	 * 
	 * @param url
	 *            the URL to ratebeer as a string
	 */
	public void setRatebeerURL(String url) {
		ratebeerURL = url;
	}

	/**
	 * Provides the ratebeer URL
	 * 
	 * @return the ratebeer URL, or <code>null</code> if it doesn't exist.
	 */
	public String getRatebeerURL() {
		return ratebeerURL;
	}
	
	@Override
	public String toString() {
		String brewery = getBrewery();
		String name = getName();
		String res;
		if (brewery != null && brewery.length() > 0) {
			res = brewery + " " + name;
		} else {
			res = name;
		}
		return res;
	}

}
