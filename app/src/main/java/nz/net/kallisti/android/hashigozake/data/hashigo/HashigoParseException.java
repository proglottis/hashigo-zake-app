/**
 * 
 */
package nz.net.kallisti.android.hashigozake.data.hashigo;


/**
 * @author robin
 *
 */
public class HashigoParseException extends Exception {

	private static final long serialVersionUID = 4918022945579970L;
	
	/**
	 * Indicates that an error occurred while parsing the XML
	 */
	public static final int ERROR_PARSE = 0x1;

	/**
	 * Indicates that an error occurred while loading the XML data
	 */
	public static final int ERROR_IO = 0x2;
	
	private int errorType;

	public HashigoParseException(int errorType, Throwable e) {
		super(e);
		this.errorType = errorType;
	}

	/**
	 * @return the errorType
	 */
	public int getErrorType() {
		return errorType;
	}

	
}
