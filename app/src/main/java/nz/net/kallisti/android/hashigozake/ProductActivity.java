package nz.net.kallisti.android.hashigozake;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;

public class ProductActivity extends AppCompatActivity implements ProductFragment.OnFragmentInteractionListener {
    public static final String TAG = ProductActivity.class.getSimpleName();
    public static final String TITLE = "nz.net.kallisti.android.hashigozake.title";
    public static final String PRODUCT = "nz.net.kallisti.android.hashigozake.product";

    @Bind(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsingToolbar;
    @Bind(R.id.subtitle) TextView subtitle;
    @Bind(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        HashigoProduct product = (HashigoProduct) intent.getSerializableExtra(PRODUCT);

        collapsingToolbar.setTitle(product.getName());
        subtitle.setText(product.getBrewery());

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, ProductFragment.newInstance(product))
                .commit();
    }
}
