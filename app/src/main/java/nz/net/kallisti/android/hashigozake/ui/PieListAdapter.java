package nz.net.kallisti.android.hashigozake.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.R;
import nz.net.kallisti.android.hashigozake.data.pies.Pie;

/**
 * Created by james on 9/09/15.
 */
public class PieListAdapter extends RecyclerView.Adapter<PieListAdapter.ViewHolder> {

    private List<Pie> pies;

    public PieListAdapter() {
        this.pies = Collections.emptyList();
        setHasStableIds(true);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pie_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindView(pies.get(position));
    }

    @Override
    public long getItemId(int position) {
        return pies.get(position).getName().hashCode();
    }

    @Override
    public int getItemCount() {
        return pies.size();
    }

    public void updateAll(List<Pie> pies) {
        this.pies = pies;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.pie_list_item) TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindView(Pie pie) {
            name.setText(pie.getName());
        }
    }
}
