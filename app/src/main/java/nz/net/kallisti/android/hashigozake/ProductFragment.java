package nz.net.kallisti.android.hashigozake;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoServing;
import nz.net.kallisti.android.hashigozake.ui.ProductAdapter;


public class ProductFragment extends Fragment implements ProductAdapter.ProductLinkClickListener {
    private static final String TAG = ProductFragment.class.getSimpleName();
    private static final String ARG_PRODUCT = "product";

    @Bind(R.id.list) RecyclerView list;

    private HashigoProduct product;
    private OnFragmentInteractionListener listener;

    public static ProductFragment newInstance(HashigoProduct product) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PRODUCT, product);
        fragment.setArguments(args);
        return fragment;
    }

    public ProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            product = (HashigoProduct) getArguments().getSerializable(ARG_PRODUCT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        final ProductAdapter adapter = new ProductAdapter(product, this);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case ProductAdapter.ITEM_LINKS:
                        return 2;
                    default:
                        return 1;
                }
            }
        });
        list.setLayoutManager(manager);
        list.setAdapter(adapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onProductLinkClick(ProductAdapter.LinkType type, HashigoProduct product) {
        Intent intent;
        switch (type) {
            case UNTAPPD:
                intent = new Intent(Intent.ACTION_VIEW, BeerListUtils.getUntappdUri(getActivity(), product));
                startActivity(intent);
                break;
            case RATEBEER:
                intent = new Intent(Intent.ACTION_VIEW, BeerListUtils.getRatebeerUri(getActivity(), product));
                startActivity(intent);
                break;
        }
    }

    public interface OnFragmentInteractionListener {
    }
}
