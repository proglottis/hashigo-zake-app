package nz.net.kallisti.android.hashigozake.data.hashigo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct.PouringStatus;

/**
 * This represents the beer data from Hashigo Zake
 * 
 * @author robin
 */
public class HashigoProducts implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6902765997530249608L;
	@SuppressWarnings("unused")
	private static final String DEBUG_TAG="HashigoProducts";

	private String timestamp;

	private ArrayList<HashigoProduct> beer = new ArrayList<HashigoProduct>();

	private HashMap<PouringStatus, List<HashigoProduct>> productsByPouring = new HashMap<HashigoProduct.PouringStatus, List<HashigoProduct>>();

	private String lastOn;

	/**
	 * Get the timestamp of this data, e.g. "16:29 on 05/08/2012"
	 * 
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the timestamp for this data, in the same form as
	 * {@link #getTimestamp()}
	 * 
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Provides a list of all the products
	 * 
	 * @return the products
	 */
	public List<HashigoProduct> getAllProducts() {
		return beer;
	}

	/**
	 * Provides the
	 * 
	 * @param status
	 * @return
	 */
	public List<HashigoProduct> getProductsWhere(PouringStatus status) {
		List<HashigoProduct> pr;
		if ((pr = productsByPouring.get(status)) == null) {
			ArrayList<HashigoProduct> prods = new ArrayList<HashigoProduct>();
			for (HashigoProduct p : beer) {
				if (status == p.getPouringStatus())
					prods.add(p);
			}
			productsByPouring.put(status, prods);
			return prods;
		}
		return pr;
	}

	/**
	 * Adds a product
	 * 
	 * @param product
	 *            the product to add
	 */
	public void addProduct(HashigoProduct product) {
		this.beer.add(product);
		if (isLastOn(product))
			product.setNewest();
		if (productsByPouring.size() != 0) {
			productsByPouring = new HashMap<PouringStatus, List<HashigoProduct>>();
		}
	}

	/**
	 * This sets the product that was last on. Due to data source constraints,
	 * it's a single string that is the brewery followed by the beer, separated
	 * with a space.
	 * 
	 * @param lastOn
	 *            the beer that's last on, in the form of brewery+" "+beer
	 */
	public void setLastOn(String lastOn) {
		this.lastOn = lastOn;
	}

	/**
	 * Provides the beer that last came on. It is in the form brewery+" "+beer.
	 * 
	 * @return the last beer on
	 */
	public String getLastOn() {
		return lastOn;
	}
	
	/**
	 * Determine if this is the newest thing on.
	 * @param product the product to test
	 * @return <code>true</code> if the product is the newest thing on, <code>false</code> otherwise.
	 */
	private boolean isLastOn(HashigoProduct product) {
		if (lastOn == null || lastOn.trim().equals(""))
			return false;
		String brewery = product.getBrewery();
		String name = product.getName();
		String test;
		// This handles the case where the brewery isn't supplied
		if (brewery != null && !brewery.trim().equals("")) {
			test = brewery + " " + name;
		} else {
			test = name;
		}

		if (test.equals(getLastOn()))
			return true;
		return false;
	}

}
