/*	Copyright (C) 2012 robin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package nz.net.kallisti.android.hashigozake;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Handles the settings activity
 * 
 * @author robin
 */
public class SettingsActivity extends AppCompatActivity {
	public static final String PREF_NOTIFY_DEVICE="pref_notify_device";
	public static final String PREF_NOTIFY_PEBBLE="pref_notify_pebble";
	public static final String PREF_NOTIFY_GEOLOCATE="pref_notify_geolocate";
	public static final String PREF_SOBA_MEMBER="pref_soba_member";

    @Bind(R.id.toolbar) Toolbar toolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getFragmentManager().beginTransaction()
                .replace(R.id.content, new SettingsFragment())
                .commit();
	}
}
