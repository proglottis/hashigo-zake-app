package nz.net.kallisti.android.hashigozake.data;

import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProducts;

/**
 * Created by james on 8/09/15.
 */
public interface HashigoProductsListener {
    void onProductsRequested();
    void onProductsResponse();
    void onProducts(HashigoProducts products);
    void onProductsError();
}
