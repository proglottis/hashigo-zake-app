/*	Copyright (C) 2013 Robin Sheat

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nz.net.kallisti.android.hashigozake.data.pies;

import java.io.Serializable;

/**
 * This represents a single pie.
 * 
 * @author Robin Sheat
 */
public class Pie implements Serializable {

	private static final long serialVersionUID = 983315862200724780L;
	private String name;
	private boolean available;

	public Pie(String name, boolean available) {
		this.name = name;
		this.available = available;
	}

	public String getName() {
		return name;
	}

	public boolean isAvailable() {
		return available;
	}

}
