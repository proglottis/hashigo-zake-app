package nz.net.kallisti.android.hashigozake;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.HashigoProductsListener;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProducts;
import nz.net.kallisti.android.hashigozake.ui.BottleListAdapter;


public class BottleListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, HashigoProductsListener,
        ExpandableListView.OnChildClickListener {
    public static final String TAG = BottleListFragment.class.getSimpleName();
    private static final String SORT_ORDER_KEY = "SORT_ORDER_KEY";

    @Bind(R.id.progress) ProgressBar progress;
    @Bind(R.id.refresh_layout) SwipeRefreshLayout refreshLayout;
    @Bind(R.id.list) ExpandableListView list;

    private OnFragmentInteractionListener listener;
    private BottleListAdapter adapter;

    public static BottleListFragment newInstance() {
        return new BottleListFragment();
    }

    public BottleListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_bottle_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bottle_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


        adapter = new BottleListAdapter(getActivity());
        list.setAdapter((ExpandableListAdapter) adapter);
        list.setOnChildClickListener(this);
        refreshLayout.setOnRefreshListener(this);
        listener.refreshBottleList(this, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_refresh:
                listener.refreshBottleList(this, true);
                return true;
            case R.id.menu_sort_brewery:
                setSort(BottleListAdapter.SortBy.BREWERY);
                return true;
            case R.id.menu_sort_style:
                setSort(BottleListAdapter.SortBy.STYLE);
                return true;
            case R.id.menu_sort_name:
                setSort(BottleListAdapter.SortBy.ALPHABETICAL);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        listener.refreshBottleList(this, true);
    }

    @Override
    public void onProductsRequested() {
        if(!refreshLayout.isRefreshing()) {
            progress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onProductsResponse() {
        progress.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onProducts(HashigoProducts products) {
        if (getActivity() == null) {
            return;
        }
        int sortOrder = PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getInt(SORT_ORDER_KEY, BottleListAdapter.SortBy.BREWERY.ordinal());
        adapter.updateAll(products, BottleListAdapter.SortBy.values()[sortOrder]);
    }

    @Override
    public void onProductsError() {
        progress.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        HashigoProduct product = (HashigoProduct) adapter.getChild(groupPosition, childPosition);
        if (product != null) {
            listener.onProductSelected(product);
            return true;
        }
        return false;
    }

    private void setSort(BottleListAdapter.SortBy sortBy) {
        if (getActivity() == null) {
            return;
        }
        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                .putInt(SORT_ORDER_KEY, sortBy.ordinal())
                .apply();
        adapter.setCurrentSort(sortBy);
    }

    public interface OnFragmentInteractionListener extends ProductSelectedListener {
        void refreshBottleList(HashigoProductsListener listener, boolean force);
    }

}
