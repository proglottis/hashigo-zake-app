package nz.net.kallisti.android.hashigozake;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.PiesListener;
import nz.net.kallisti.android.hashigozake.data.pies.Pie;
import nz.net.kallisti.android.hashigozake.data.pies.Pies;
import nz.net.kallisti.android.hashigozake.ui.PieListAdapter;


public class PieListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, PiesListener {
    public static final String TAG = PieListFragment.class.getSimpleName();

    @Bind(R.id.progress) ProgressBar progress;
    @Bind(R.id.refresh_layout) SwipeRefreshLayout refreshLayout;
    @Bind(R.id.list) RecyclerView list;

    private OnFragmentInteractionListener listener;
    private PieListAdapter adapter;

    public static Fragment newInstance() {
        return new PieListFragment();
    }

    public PieListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pie_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        adapter = new PieListAdapter();
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(adapter);
        refreshLayout.setOnRefreshListener(this);
        listener.refreshPieList(this, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_refresh:
                listener.refreshPieList(this, true);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        listener.refreshPieList(this, true);
    }

    @Override
    public void onPiesRequested() {
        if(!refreshLayout.isRefreshing()) {
            progress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPiesResponse() {
        progress.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onPies(Pies pies) {
        List<Pie> available = pies.getAvailablePies();
        adapter.updateAll(available);
    }

    @Override
    public void onPiesError() {
        progress.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    public interface OnFragmentInteractionListener {
        void refreshPieList(PiesListener listener, boolean force);
    }
}
