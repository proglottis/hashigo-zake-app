package nz.net.kallisti.android.hashigozake;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;

import net.peterkuterna.android.apps.pinnedheader.PinnedListView;

import butterknife.Bind;
import butterknife.ButterKnife;
import nz.net.kallisti.android.hashigozake.data.HashigoProductsListener;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProduct;
import nz.net.kallisti.android.hashigozake.data.hashigo.HashigoProducts;
import nz.net.kallisti.android.hashigozake.ui.BottleListAdapter;
import nz.net.kallisti.android.hashigozake.ui.ProductListAdapter;


public class BeerListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, HashigoProductsListener,
        AdapterView.OnItemClickListener {
    public static final String TAG = BeerListFragment.class.getSimpleName();

    @Bind(R.id.progress) ProgressBar progress;
    @Bind(R.id.refresh_layout) SwipeRefreshLayout refreshLayout;
    @Bind(R.id.list) PinnedListView list;

    private OnFragmentInteractionListener listener;
    private ProductListAdapter adapter;

    public static BeerListFragment newInstance() {
        return new BeerListFragment();
    }

    public BeerListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_beer_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean forceUpdate = preferences.getBoolean(GCMIntentService.UPDATE_GCM_KEY, false);
        if(forceUpdate) {
            preferences.edit().remove(GCMIntentService.UPDATE_GCM_KEY).apply();
        }

        adapter = new ProductListAdapter(getActivity());
        list.setAdapter(adapter);
        list.setOnScrollListener(adapter);
        list.setPinnedHeaderView(LayoutInflater.from(getActivity()).inflate(R.layout.beer_list_item_header, list, false));
        list.setOnItemClickListener(this);
        refreshLayout.setOnRefreshListener(this);
        listener.refreshBeerList(this, forceUpdate);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_refresh:
                listener.refreshBeerList(this, true);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        listener.refreshBeerList(this, true);
    }

    @Override
    public void onProductsRequested() {
        if(!refreshLayout.isRefreshing()) {
            progress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onProductsResponse() {
        progress.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onProducts(HashigoProducts products) {
        adapter.updateAll(products);
    }

    @Override
    public void onProductsError() {
        progress.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listener.onProductSelected((HashigoProduct) adapter.getItem(position));
    }

    public interface OnFragmentInteractionListener extends ProductSelectedListener {
        void refreshBeerList(HashigoProductsListener listener, boolean force);
    }
}
