package nz.net.kallisti.android.hashigozake.data;

import nz.net.kallisti.android.hashigozake.data.pies.Pies;

/**
 * Created by james on 9/09/15.
 */
public interface PiesListener {
    void onPiesRequested();
    void onPiesResponse();
    void onPies(Pies pies);
    void onPiesError();
}
