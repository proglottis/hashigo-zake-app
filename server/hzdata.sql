---
--- Holds the registered GCM device identifiers
---
CREATE TABLE gcm_regids (
    regid       TEXT NOT NULL,
    registered  BOOLEAN DEFAULT 1,
    timestamp   TIMESTAMP DEFAULT now(),
    INDEX regid_key (regid(255))
);
