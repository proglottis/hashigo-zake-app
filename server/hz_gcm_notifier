#!/usr/bin/perl 

# A perl script that performs GCM "update" notifications for the HZ app

#    Copyright (C) 2012 Robin Sheat
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

=head1 hz_gcm_notifier - sends a GCM notification containing 'update=1' to
all the IDs in a database.

=head1 USAGE

B<hz_gcm_notifier>
B<--file>=I<data file>
B<--config>=I<file>
B<--debug>
B<--help>
B<--man>

=head1 OPTIONS

=item B<file>=I<data file>

If provided, this points to the file that contains the output file that
devices fetch. This is used to get any extra data to be included in the
GCM message.

=item B<config>=I<file>

Specifies the config files to use to work out what GCM keys to use and how
to access the database.

=item B<--debug>

Outputs informational messages to the terminal when running, normally this runs
silently except for syslogging errors.

=item B<--help>

A summary of usage

=item B<--man>

Detailed usage

=head1 CONFIGURATION FILE

This uses the following parameters from the config file:

    dbi_data_source=dbi:mysql:hzdata
    dbi_username=hzdata
    dbi_password=password
    gcm_server_key=server_key 

=head1 AUTHOR

Robin Sheat <robin@kallisti.net.nz>

=cut

use strict;
use warnings;

use Data::Dumper;

use DBI;
use File::Basename;
use Getopt::Long;
use HTTP::Request;
use JSON;
use LWP::UserAgent;
use Pod::Usage;
use Proc::Daemon;
use Sys::Syslog qw/ :standard :macros /;

my ( $data_file, $config_file );
my %options = (
    'file'   => \$data_file,
    'config' => \$config_file,
    'debug'  => 0,
);
my $opt_res = GetOptions( \%options, 'file=s', 'config=s', 'debug', 'help', 'man' );
pod2usage( { -verbose => 0, -exitval => 1 } ) if !$opt_res;
pod2usage( { -verbose => 0 } ) if $options{help};
pod2usage( { -verbose => 2 } ) if $options{man};

if ( !$options{debug} ) {
    openlog( basename($0), 'pid', "LOG_USER" );
    setlogmask(
        LOG_INFO | LOG_NOTICE | LOG_WARNING | LOG_ERR | LOG_CRIT | LOG_ALERT |
          LOG_EMERG );
}

my %config = load_config($config_file);

my $dbh = DBI->connect( $config{dbi_data_source},
    $config{dbi_username}, $config{dbi_password},
    { AutoCommit => 1, RaiseError => 1, PrintError => 0 } );
my $sth     = $dbh->prepare("SELECT regid FROM gcm_regids WHERE registered=1");
my $sth_res = $sth->execute;
if ( !$sth_res ) {
    _syslog( "LOG_ERR", "Failed to fetch from database: $sth->err" );
    die;
}

# Now we load the gcm keys from the database, batch them up in to lots of 1,000
# (the GCM limit) and fire them off.
my @gcm_batch;
my $count = 0;
_syslog( "LOG_DEBUG", "About to start looping over regids" );
while ( my $row = $sth->fetchrow_arrayref ) {
    $count++;
    push @gcm_batch, $row->[0];
    _syslog( "LOG_DEBUG", "Noting $row->[0]" );
    if ( $count >= 1000 ) {
        send_gcm_to( \@gcm_batch );
        @gcm_batch = ();
        $count     = 0;
    }
}
if (@gcm_batch) {
    send_gcm_to( \@gcm_batch );
}

# This sends an update message to the supplied IDs. It catches the response
# and updates the database if any of the responses are marked as no longer
# registered. It expects a list of 1,000 or fewer IDs.
#
# Usage:
#   send_gcm_to(\@gcm_ids);
#
sub send_gcm_to {
    my ($gcm_ids) = @_;

    my $api_key  = $config{gcm_server_key};
    my $api_url  = 'https://android.googleapis.com/gcm/send';
    my $gcm_data = {
        registration_ids => $gcm_ids,
        collapse_key     => 'update',
        data             => { update => 1, },
    };
    my $latest = get_latest_from_file($data_file);
    $gcm_data->{data}{latest} = $latest if ($latest);
    my $gcm_json = gcm_json_encode($gcm_data);
    _syslog( "LOG_DEBUG", "Sending JSON to GCM: $gcm_json" )
      if ( $options{debug} );
    my $req = HTTP::Request->new( POST => $api_url );
    $req->header( 'Authorization' => 'key=' . $api_key );
    $req->header( 'Content-Type'  => 'application/json; charset=UTF-8' );
    $req->content($gcm_json);
    my $ua  = LWP::UserAgent->new;
    my $res = $ua->request($req);
    _syslog( "LOG_DEBUG", "Got response: " . $res->content );
    my %gcm_res = gcm_decode($res);
    process_gcm_response( \%gcm_res, $gcm_ids );
}

# Gets the latest on tap from the data file
sub get_latest_from_file {
    my ($file) = @_;

    # I don't want to use an XML parser
    my $fh;
    if ( !open $fh, '-|', 'xmlstarlet', 'sel', '-t', '-v',
        'Products/LastChange/On', $file )
    {
        warn "Failed to exec xmlstarlet on $file: $!";
        return undef;
    }
    chomp( my @res = <$fh> );
    close $fh;
    return undef if !@res;
    # For some reason, xmlstarlet doesn't do this right
    $res[0] =~ s/&amp;/&/g;
    $res[0] =~ s/&lt;/</g;
    $res[0] =~ s/&gt;/>/g;

    return $res[0];
}

# Convert to JSON acceptable to GCM
sub gcm_json_encode {
    my ($payload) = @_;

    my $gcm_uri = 'https://android.googleapis.com/gcm/send';
    if ( exists( $payload->{delay_while_idle} ) ) {
        $payload->{delay_while_idle} =
          $payload->{delay_while_idle} ? JSON::True : JSON::False;
    }
    my $gcm_json = objToJson($payload);
    return $gcm_json;
}

# Decode the response from GCM into a useful hash
sub gcm_decode {
    my ($res) = @_;

    my %data;
    if ( !$res->is_success ) {
        $data{error} = $res->content;
        $data{code}  = $res->status_line;
        return %data;
    }
    $data{content} = jsonToObj( $res->content );
    return %data;
}

# This processes the GCM response and handles anything that needs handling
sub process_gcm_response {
    my ( $gcm, $ids ) = @_;

    # First check and log errors
    if ( exists( $gcm->{error} ) ) {
        _syslog( "LOG_ERR",
            "Error from GCM: status: $gcm->{code}, content: $gcm->{error}" );
        return;
    }
    my $cont = $gcm->{content};
    return if ( !( $cont->{failure} || $cont->{canonical_ids} ) );    # yay!
        # This now follows the recommended procedure for processing the response
    my $results = $cont->{results};
    if ( @$results != @$ids ) {
        _syslog( "LOG_ERR",
            "The response and source ID counts differ: "
              . Dumper( $ids, $gcm ) );
        return;
    }
    foreach my $orig_id (@$ids) {
        my $r = shift @$results;
        if ( exists $r->{message_id} ) {
            my $new_id = $r->{registration_id};
            if ($new_id) {
                update_id( $orig_id, $new_id );
                next;
            }
        }
        else {
            my $err = $r->{error};
            next if !$err;

            # Our messages aren't important enough to bother resending
            # if there's a failure, so we'll log it and move on.
            if ( $err eq 'Unavailable' ) {
                _syslog( "LOG_WARNING", "Should resend GCM request: $err" );
                next;
            }
            _syslog( "LOG_DEBUG", "Removing GCM id due to $err" );
            remove_gcm_id( $orig_id, $err );
        }
    }
}

# Marks a GCM ID as unused
sub remove_gcm_id {
    my ( $id, $note ) = @_;

    $note ||= '';
    $dbh->do( 'UPDATE gcm_regids SET registered=0, note=? WHERE regid=?',
        undef, $note, $id );
}

# Updates a GCM ID, if we've been told we need to convert an old one to a new
# one.
#
# Usage:
#   update_id($orig_id, $new_id);
#
sub update_id {
    my ( $orig_id, $new_id ) = @_;

    _syslog( "LOG_DEBUG", "New canonical ID: $orig_id -> $new_id" );

    # We need to account for the possibility that the new ID we've been given
    # is already in the system.
    my $check_sth =
      $dbh->prepare('SELECT COUNT(*) FROM gcm_regids WHERE regid=?');
    $check_sth->execute($new_id);
    my $is_present = $check_sth->fetchrow_arrayref->[0];
    if ($is_present) {
        _syslog( "LOG_DEBUG",
            "Canonical ID already registered, removing old one" );
        remove_gcm_id( $orig_id, 'Canonical ID already registered' );
        return;
    }
    _syslog( "LOG_DEBUG", "Canonical ID not present, updating" );
    my $update_sth =
      $dbh->prepare('UPDATE gcm_regids SET regid=? WHERE regid=?');
    $update_sth->execute( $new_id, $orig_id );
}

# This loads the configuration file into a hash.
#
# Usage:
#   %config = load_config($config_file);
#
sub load_config {
    my ($file) = @_;

    if ( !$file ) {
        die("No file defined. Try using the --config option");
    }
    open my $fh, '<', $file or die "Unable to open $file: $!\n";
    my %opts;
    while (<$fh>) {
        s/^(.*?)#/$1/;
        my ( $o, $v ) = m/^(.*?)=(.*)$/;
        next if !$o || !$v;
        $opts{$o} = $v;
    }
    return %opts;
}

# Logs stuff to syslog or to the terminal.
#
# Usage:
#   _syslog($priority, $message)
#
# $priority takes syslog priorities, e.g. 'LOG_WARN'.
#
sub _syslog {
    my ( $priority, $message ) = @_;
    if ( $options{debug} ) {
        print STDERR "$priority\t$message\n";
    }
    else {
        syslog( $priority, $message );
    }
}

